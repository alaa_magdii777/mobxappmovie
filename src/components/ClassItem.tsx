import { Card, Paragraph, Title } from "react-native-paper";
import * as React from "react";
import { FC } from "react";

export const ClassItem: FC<{
  onPress: () => void;
  name: string;
}> = ({onPress, name}) => {
  return <Card onPress={onPress}>
    {name? <Card.Content>
   <Title>Hello {name}</Title>
      {/* <Paragraph>{`My ${name.toLocaleLowerCase()} classes details`}</Paragraph> */}
      {/* <Paragraph>{`My ${name?.toLocaleLowerCase()} classes details`}</Paragraph> */}
    </Card.Content>:null}
  </Card>;
}
