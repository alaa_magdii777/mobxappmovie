/**
 * Created by Dima Portenko on 24.11.2021
 */
import React from "react";
import {
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
  Text,
  StyleSheet
} from "react-native";
import { Appbar, TextInput, useTheme } from "react-native-paper";
import { RootStackScreenProps } from "../types";
import * as routes from "../navigation/routes";
import { useStore } from "../store";

type LoginClassScreenProps = RootStackScreenProps<typeof routes.LOGIN_CLASS> & {};

export const LoginClassScreen = ({
  navigation,
  route,
}: LoginClassScreenProps) => {
  const item = route.params?.classItem ?? {
    id: new Date().getTime(),
    name: "",
    password: "",
  };
  const { classes } = useStore();

  const theme = useTheme();
  const {
    colors: { background },
  } = theme;

  const [name, setName] = React.useState(item.name);
  const [password, setPassword] = React.useState(item.password);
  const [errorUserName,setErrorUserName] = React.useState(false);
  const [errorPassword, setErrorPassword] = React.useState(false);

  // const onDelete = () => {
  //   classes.deleteItem(item);
  //   navigation.goBack();
  // };

  const onSave = () => {
    if (!name) {
      setErrorUserName(true);
    } 
    if (!password) {
      setErrorPassword(true);
    } 
    else {
      classes.updateItem({
        id: item.id,
        name,
        password,
      });
      navigation.navigate(routes.HOME);
    }
  };

  return (
    <KeyboardAvoidingView style={{ flex: 1 }}>
      <Appbar.Header theme={theme}>
        <Appbar.BackAction onPress={navigation.goBack} />
        <Appbar.Content title="Login Screen" />
        {/* <Appbar.Action icon="delete" onPress={onDelete}/> */}
        {/* <Appbar.Action icon="content-save" onPress={onSave}/> */}
      </Appbar.Header>
      <View style={{ flex: 1, backgroundColor: background, padding: 15 }}>
        <TextInput
          mode="outlined"
          label="user name"
          value={name}
          error={errorUserName}
          onChangeText={(text) => {
            setName(text);
            setErrorUserName(false);
          }}
        />
        {errorUserName?<Text style={styles.errorTxt}>please input a username</Text>:null}
        <TextInput
          mode="outlined"
          label="password"
          value={password}
          error={errorPassword}
          onChangeText={(text) => {
            setPassword(text);
            setErrorPassword(false);
          }}
        />
        {errorPassword?<Text style={styles.errorTxt}>please input a password</Text>:null}
        <TouchableOpacity
          style={styles.btn}
          onPress={onSave}>
          <Text style={{ color: "white", fontSize: 18 }}>Submit</Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  );
};

const styles=StyleSheet.create({
btn:{
  backgroundColor: "black",
  height: 50,
  width: "100%",
  marginVertical: 40,
  justifyContent: "center",
  alignItems: "center",
},
errorTxt:{color:"darkred",fontSize:12}
})