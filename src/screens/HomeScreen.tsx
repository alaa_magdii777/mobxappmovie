import * as React from "react";
import { FAB } from "react-native-paper";
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Image,
  ActivityIndicator,
} from "react-native";
import { RootStackScreenProps } from "../types";
import * as routes from "../navigation/routes";
import { ClassItem } from "../components/ClassItem";
import { useStore } from "../store";
import { observer } from "mobx-react-lite";
import { useNavigation } from "@react-navigation/native";
import { Appbar, useTheme } from "react-native-paper";
type Props = { keyMovies: any } & RootStackScreenProps<typeof routes.HOME>;

export const HomeScreen = observer(({ route }: Props) => {
  const item = route.params?.classItem ?? {
    id: "",
    name: "",
    password: "",
  };
  console.log(route, "item");
  const rootStore = useStore();
  const navigation = useNavigation();
  const [keyMovies, setKeyMovies] = React.useState("");
  React.useEffect(() => {
    fetch("http://www.omdbapi.com?apikey=741fd8d3&t=car", {
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setKeyMovies(data);
        // console.log(data);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);
  // console.log(keyMovies, "keyMovies");
  const theme = useTheme();
  const {
    // colors: { background },
  } = theme;
  const { classes } = useStore();
  const onDelete = () => {
    classes.deleteItem(item);
    navigation.navigate(routes.LOGIN_CLASS);
    // navigation.goBack();
    // if (navigation.canGoBack()) navigation.goBack();
    // else BackHandler.exitApp();
  };
  return (
    <View style={styles.container}>
      <Appbar.Header theme={theme}>
        <Appbar.Action icon="logout" onPress={onDelete} />
      </Appbar.Header>
      <ScrollView>
        {rootStore.classes.items.map((item) => {
          return (
            <View key={`${item.id}`} style={{ marginBottom: 15 }}>
              <ClassItem
                name={item.name}
                onPress={() => {
                  navigation.navigate(routes.LOGIN_CLASS, {
                    classItem: item,
                  });
                }}
              />
            </View>
          );
        })}
       {!keyMovies?<ActivityIndicator size="small" color={"purple"}/>
       :
        <View style={styles.cartMovie}>
          <Image
            //@ts-ignore
            source={{ uri: keyMovies.Poster }}
            style={styles.posterImg}
          />
          <View style={{ padding: 10 }}>
            <Text style={{ color: "purple", fontSize: 20, fontWeight: "700" }}>
              {
                //@ts-ignore
                keyMovies.Title
              }
            </Text>
            <>
              {
                //@ts-ignore
                keyMovies?.Ratings?.map((e) => (
                  <View style={styles.row}>
                    <Text style={styles.source}>{e?.Source}</Text>
                    <Text style={styles.rating}>{e?.Value}</Text>
                  </View>
                ))
              }
            </>
            <Text style={{ color: "#5799ef", marginTop: 10 }}>
              {
                //@ts-ignore
                keyMovies?.Actors
              }
            </Text>
          </View>
        </View>}
      </ScrollView>
      {/* <FAB
        style={styles.fab}
        icon="plus"
        onPress={() => {
          navigation.navigate(routes.LOGIN_CLASS);
        }}
      /> */}
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: "black",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  fab: {
    position: "absolute",
    margin: 32,
    right: 0,
    bottom: 0,
  },
  posterImg: {
    height: 200,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  rating: {
    color: "#ffc60a",
  },
  source: {
    color: "white",
  },
  cartMovie: {
    width: "100%",
    borderRadius: 15,
    borderColor: "lightgray",
    borderWidth: 1,
    paddingBottom: 30,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    // alignItems:"center"
  },
});
